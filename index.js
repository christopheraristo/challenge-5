const express = require('express')
const app = express()
const fs = require('fs')

const userList = "userList.txt"

app.use(express.urlencoded());
app.use('/assets', express.static('assets'));

app.get('/', (req,res) => {res.sendFile(__dirname + '/views/home.html')})
app.get('/game', (req,res) => {res.sendFile(__dirname + '/views/rps.html')})

app.get('/login', (req,res) => {res.sendFile(__dirname + '/views/login.html')});
app.post('/login', login);

app.get('/userinfo/', responseHeader, getUserData);
app.get('/userinfo/:id', responseHeader, getUserData);
app.post('/userinfo/:username/:password', responseHeader, insertUserData)

function getList(path, callback){
    fs.readFile(path, 'utf-8', (err, data)=>{
        let list = JSON.parse(data);
        callback(list)
    })
}

function getUserData(req, res){
    getList(userList, (data)=>{
        if(req.params.id)
            return res.json(data[req.params.id])
        else
            return res.json(data)
    })
}

function responseHeader(req, res, next){
    res.setHeader('Content-Type', 'application/json');
    next()
}

function login(req, res){

    let username = req.body.username

    getList(userList, (userList)=>{

        let found = undefined
        for(index in userList){
            if(username == userList[index].username){
                found = index
                break
            }
        }

        if(!found){
            res.status(400)
            return res.end('Username not found');
        }
        if(req.body.password != userList[index].password){
            res.status(400)
            return res.end('Incorrect Password')
        }

        res.status(200)
        return res.end('Welcome ' + userList[found].username)
    })
}

function insertUserData(req,res){

    username = req.params.username
    password = req.params.password
    
    getList(userList, (list)=>{

        let found = undefined
        for(index in list){
            if(username == list[index].userName){
                found = index
                break
            }
        }

        if(found){
            res.status(400)
            return res.end('Username already exist');
        } else {
            list.push(
                {
                    "username" : username,
                    "password" : password
                }
            )

            fs.writeFile(userList, JSON.stringify(list), (err) => {
                res.status(200)
                return res.end('Insert Success')
            })
        }
    })
}

app.listen(8000, () => console.log("Server Running"))