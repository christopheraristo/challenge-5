
// DOM Casting
const matchResult = document.getElementById("result");
const versusMiddle = document.getElementById("versus");
const rock_div = document.getElementById("player-rock");
const paper_div = document.getElementById("player-paper");
const scissor_div = document.getElementById("player-scissor");
const compChoice = document.getElementsByClassName("compImage");
const resetButton = document.querySelector(".reset");

function getComputerChoice() {
    const choices = ['r', 'p', 's'];
    const randomNumber = Math.floor(Math.random() * 3);
    return choices[randomNumber];
}

// function setCompGreyBox(computerChoice) {
//     if (getComputerChoice === 'r') {compChoice[0].classList.add('greyBoxes')} else if (getComputerChoice === 'p') {compChoice[1].classList.add('greyBoxes')} else {compChoice[2].classList.add('greyBoxes')};
// }


function win(user, computer) {
    matchResult.innerHTML = "PLAYER 1 WIN";
    document.getElementById("result").classList.add('result');
    document.getElementById("versus").innerHTML = "";
    // setCompGreyBox();
    // const smallUserWord = "user".fontsize(1).sub();
    // const smallCompWord = "computer".fontsize(1).sub();
    // matchResult.innerHTML = `${convertToWord(user)}${smallUserWord} beats ${convertToWord(computer)}${smallCompWord} . You win!`;
}

function lose() {
    matchResult.innerHTML = "COM WIN";
    document.getElementById("result").classList.add('result');
    document.getElementById("versus").innerHTML = "";
    // setCompGreyBox();
}

function draw() {
    matchResult.innerHTML = "DRAW";
    document.getElementById("result").classList.add('result');
    document.getElementById("versus").innerHTML = "";
    // setCompGreyBox();
}

function game(userChoice) {
    const computerChoice = getComputerChoice();
    switch (userChoice + computerChoice) {
        case "rs":
        case "pr":
        case "sp":
            win(userChoice, computerChoice);
            break;
        case "rp":
        case "ps":
        case "sr":
            lose(userChoice, computerChoice);
            break;
        case "rr":
        case "pp":
        case "ss":
            draw(userChoice, computerChoice);
            break;
    }
    function setCompGreyBox() {
        if (computerChoice === 'r') {compChoice[0].classList.add('greyBoxes')} else if (computerChoice === 'p') {compChoice[1].classList.add('greyBoxes')} else {compChoice[2].classList.add('greyBoxes')};
    }
    setCompGreyBox();
}

function main() {
    rock_div.addEventListener('click', function() {
        game("r");
        document.getElementById("player-rock").classList.add('greyBoxes');
    })
    
    paper_div.addEventListener('click', function() {
        game("p");
        document.getElementById("player-paper").classList.add('greyBoxes');
    })
    
    scissor_div.addEventListener('click', function() {
        game("s");
        document.getElementById("player-scissor").classList.add('greyBoxes');
    })

    resetButton.addEventListener('click', function() {
        window.location.reload();
    })
}

main();
